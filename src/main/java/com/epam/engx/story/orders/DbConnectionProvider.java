package com.epam.engx.story.orders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnectionProvider {
  private static ConnectionPool pool;

  public DbConnectionProvider(int connectionCount) {
    pool = new ConnectionPool(connectionCount);
  }

  public static Connection getDBConnection() {
    return pool.getConnection();
  }

  public static void releaseConnection(Connection connection) {
    pool.releaseConnection(connection);
  }
}
