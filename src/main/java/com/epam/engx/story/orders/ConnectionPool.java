package com.epam.engx.story.orders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionPool {
  private static final String DB_NAME = "swift_shift_db";
  private static final String USER = "user";
  private static final String PASSWORD = "never_store_passwords_in_code";

  private final List<Connection> connectionPool = new ArrayList<>();

  public ConnectionPool(int initConnections) {
    for (int i = 0; i < initConnections; i++) {
      connectionPool.add(createConnection());
    }
  }

  public Connection getConnection() {
    if (connectionPool.isEmpty()) {
      connectionPool.add(createConnection());
    }
    return connectionPool.remove(connectionPool.size() - 1);
  }

  public void releaseConnection(Connection connection) {
    connectionPool.add(connection);
  }

  private Connection createConnection() {
    try {
      return DriverManager.getConnection(DB_NAME, USER, PASSWORD);
    } catch (SQLException e) {
      throw new IllegalStateException("Failed to create DB connection");
    }
  }
}
