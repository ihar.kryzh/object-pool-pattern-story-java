package com.epam.engx.story.orders;

import java.sql.Connection;

public class OrderRepository {
  public void saveOrder(Order order) {
    Connection connection = DbConnectionProvider.getDBConnection();
    saveToDb(order, connection);
    DbConnectionProvider.releaseConnection(connection);
  }

  public Order readOrder(int orderId) {
    Connection connection = DbConnectionProvider.getDBConnection();
    Order order = readFromDb(orderId, connection);
    DbConnectionProvider.releaseConnection(connection);
    return order;
  }

  private void saveToDb(Order order, Connection connection) {
    // Implementation details to save orders to the database
  }

  private Order readFromDb(int orderId, Connection connection) {
    Order order = null;
    // Implementation details to read orders from the database
    return order;
  }
}
